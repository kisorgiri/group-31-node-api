const { urlencoded } = require('express');
const express = require('express');
const app = express(); // app is entire express framework
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');
require('./db');


// events stuff
const events = require('events');
const myEvent = new events.EventEmitter();

app.use(function (req, res, next) {
    req.myEvent = myEvent;
    next();
})

myEvent.on('err', function (err,res) {
    console.log('err in event ', err);
    res.json(err);
})

// socket stuff
require('./socket')(app);

// import routing level middleware
const APIRoute = require('./routes/api.route');

// implement CORS
app.use(cors());

// load inbuilt middleware
app.use(express.static('uploads')); // for__dirname,'uploads' erving
app.use('/file', express.static(path.join(__dirname, 'uploads'))); // for external serving
// server must parse incoming request
// parse from-encoded-data
app.use(urlencoded({
    extended: true
}));
// JSON  parser
app.use(express.json())
// this middleware will parse incoming data and add it in req.body property

// third party middleware
app.use(morgan('dev'))

// load routing level middleware
app.use('/api', APIRoute);

// catch 404
app.use(function (req, res, next) {
    next({
        msg: "Not Found",
        status: 404
    })
})

// error handling middleware must be called for execution
// next with argument will invoke error handling middleware
app.use(function (err, req, res, next) {
    console.log('error is >>', err);
    // err or 1st argument is for error
    // req, res, next ==> http request , http response , next middleware function reference
    // TODO set status code
    res.status(err.status || 400)
    res.json({
        msg: err.msg || err,
        status: err.status || 400
    })
})

app.listen(9000, function (err, done) {
    if (err) {
        console.log('server listening failed ');
    } else {
        console.log('server listening at port 9000')
        console.log('press CTRL + C to exit');
    }
})


// middleware
// middleware is a function that has access to http request object, http response object and next middleware function reference.
// middleware always cam into action in between req res cycle
// middleware can modify req and res object
// the order of middleware is very important

// syntax
// function(req,res,next){
//     // middleware function
//     // req or 1st arugment is http request
//     // res or 2nd arugment is http response object
//     // next or 3rd argument is next middleware reference

// }
// configuration
// app.use(function (req, res, next) {
// // middleware
// })

// app.use is configuaration block for middleware

// types of middleware
// 1. application level middleware
// any middleware where we have direct scope request response and next are application level middleware
// 2. routing level middleware
// 3. third party middleware
// 4. inbuilt middleware
// 5. error handling middleware
