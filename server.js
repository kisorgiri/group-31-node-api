const http = require('http');
const fileWrite = require('./fileOperation');

const server = http.createServer(function (request, response) {
    // callback for every http request
    // request or 1st argument is http request object
    // response or 2nd argument is http response object
    console.log('client connected to server');
    // console.log('incoming request >>',request);
    console.log('request url >>', request.url);
    console.log('request method >>', request.method);
    // regardless of any url and method this callback is executed for every client request
    if (request.url === '/write') {
        fileWrite('kishor.txt', 'welcome to nodejs')
            .then(function (done) {
                response.end('file writing success' + done)
            })
            .catch(function (err) {
                response.end("file writing failed >", done);
            })
    }
    else if (request.url === '/read') {

    } else {
        response.end('no any action to perform')
    }

})


server.listen(8080, function (err, done) {
    if (err) {
        console.log('server listening failed >>', err);
    } else {
        console.log('server listening at port 8080')
        console.log('press CTRL +C to exit')
    }
})
