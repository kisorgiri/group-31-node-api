const fs = require('fs');

function write(fileName, fileContent) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/test.js', 'welcome to nodejs', function (err, done) {
            if (err) {
                reject(err);
            } else {
                resolve(done);
            }
        });
    })

}

// read
// fs.readFile('./files/test.js','UTF-8',function(err,result){
//     if(err){
//         console.log('read failed',err);
//     }else{
//         console.log('read success',result);
//     }
// })
// make it functional
// export it 
// run read from another file

// try rename and remove as well

// write('a.txt', 'b')
//     .then(function (data) {
//         console.log('success >>', data);
//     })
//     .catch(function (err) {
//         console.log('error >', err);
//     })


module.exports = write;