// database ==> a container holding data
// we have Database Management system to work with database
// we have two group of DBMS

// 1. Relational Database Management System
// 2.Distributed Database Management System

// Relational Database Management System
// RDBMS
// a Table based Design
// eg Movies , ratings, reviews,users
// b. each record are called tuples/row
// c. Schema based Solution
// ==> schema validation
// d. Reletion between table exists
// e. non scalable
// f. SQL database 
// g. mysql, sqllite, postgressql, 

// 2.Distributed Database Management System
// a. Collection based design
// eg movies, users, ratings
// b. each record are called document
// a document is valid JSON
// c.schema less design
// d. relation doesnot exist
// e. highly scalable
// NoSQL not only structured Query Language
// mongodb, dynamodb,couchdb, redis

// to work with mongodb
// mongodb server must be up and running

// to use shell command
// mongo command must be used
// command 
// mongo ===> it must give us arrow head interface

// shell command
// CRUD (Create Read Update Delete)

// show dbs ==> list all the available database
// use <db_name> ==> if existing db reselect it else create new db and select it
// show collections ==> list all the available collections of selected database

// to create a collection we must insert data
// Create
// db.<collection_name>.insert({valid-json})
// db.<collection_name>.insertMany({valid-json})

// READ
// db.<collection_name>.find({query_builder})
// db.<collection_name>.find({query_builder}).pretty() // format the output
// db.<collection_name>.find({query_builder}).count() // returns count of document
// db.<collection_name>.find({query_builder},{project[inclusion or exclusion]}).limit().skip().sort()

// UPDATE
// db.<collection_name>.update({},{},{});
// 1st object is query_builder
// 2nd object will have key as $set and value as another object with updated value
// 3rd object is optional we pass options in 3rd object
// db.<collection_name>.update({condition},{$set:{data_to_be_updated}},{options})


// DELETE
// db.<collection_name>.remove({query});
// NOTE :- dont leave the query builder empty

// drop collection
// db.<collection_name>.drop();

// drop database
// db.dropDatabase();

// ODM == for document based database
// ORM == for relational database

// ODM==> Mongoose

// advantanges of using Mongooose
//1 schema based solution
    // we will be aware of properties we need to collect
    // we will be aware of information we have to generate
//2 indexing is lot more easier
// required, unique,
// 3 data type validation
// 4 middleware
// 5 methods


// ########### BACKUP & RESTORE ########################
// bson way || json and csv way

// bson way
// backup 
// command  mongodump
// mongodump ==> it will back all the available database into default dump folder
// mongodump --db<db_name> ==> backup the selected database into dump folder
// mongodump --db<db_name> --out <path_to_output_location> ==> dump the selected db into selected folder

// restore
// command mongorestore
// mongorestore==> it looks for dump folder and try to import all the database kept there
// mongorestore --drop it will drop existing database and import from backup folder
// mongorestore <path_to_source_folder>

// JSON and CSV
// backup 
// command mongoexport
// mongoexport --db<db_name> --collection<collection_name> --out <path_to_destination_with.csv_extension>
// mongoexport -d<db_name> -c<collection_name> -o <path_to_destination_with.csv_extension>

// import in json
// command mongoimport
// mongoimport --db<db_name[existing or new]> --collection<collection_name[exiting or new>] path to soruce file

// csv
// backup
// mongoexport 
// mongoexport --db <db_name> --collection<col_name> --type=csv --fields "comma, seperated proeprtyNmae" --out <path_to_destination with.csv extension>
// mongoexport --db<db_name> --collection<col_name> --type=csv --fileds 'comma seperated value' --query="{category:'accessories'}" --out <path_to_destination with.csv>


// import from csv
// mongoimport --db<db_name[new or existing]> --collection <col_name newor existing> --type=csv <path_to_source> --headerline
// ########### BACKUP & RESTORE ########################