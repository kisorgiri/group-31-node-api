const router = require('express').Router();
const authRouter = require('./../controllers/auth.controller');
const userRouter = require('./../controllers/user.controller');
const itemRouter = require('./../components/items/items.route')

// load middleware
const authenticate = require('./../middlewares/authenticate');

router.use('/auth', authRouter);
router.use('/user', authenticate, userRouter);
router.use('/item', itemRouter(authenticate));


module.exports = router;