const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ratingSchema = new Schema({
    message: String,
    point: {
        type: Number,
        min: 1,
        max: 5
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
    timestamps: true
})

const ItemSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    description: String,
    brand: String,
    category: {
        type: String,
        required: true
    },
    price: Number,
    quantity: Number,
    warrentyPeroid: String,
    warrentyStatus: Boolean,
    color: String,
    discount: {
        discountedItem: Boolean,
        discountType: {
            type: String,
            enum: ['percentage', 'quantity', 'value']
        },
        discountValue: String
    },
    size: String,
    isReturnEligible: Boolean,
    returnTimePeroidInDay: Number,
    salesDate: Date,
    modelNo: String,
    images: [String],
    vendor: { // add automatically when addin items
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    status: {
        type: String,
        enum: ['available', 'out of stock', 'booked'],
        default: 'available'
    },
    ratings: [ratingSchema],
    offers: [String],
    tags: [String]
}, {
    timestamps: true
})

const ItemModel = mongoose.model('item', ItemSchema);
module.exports = ItemModel;