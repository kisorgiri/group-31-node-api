const router = require('express').Router();
const itemCtrl = require('./items.controller');
const Uploader = require('./../../middlewares/uploader');


module.exports = function (authenticate) {
    router.route('/')
        .get(authenticate, itemCtrl.get)
        .post(Uploader.array('image'), authenticate, itemCtrl.post);

    router.route('/search')
        .get(itemCtrl.search)
        .post(itemCtrl.search);

    router.route('/add-ratings/:item_id')
        .post(authenticate, itemCtrl.addRatings);


    router.route('/:id')
        .get(authenticate, itemCtrl.getById)
        .put(Uploader.single('image'), authenticate, itemCtrl.update)
        .delete(authenticate, itemCtrl.remove);



    return router;
}

