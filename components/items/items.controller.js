const ItemModel = require('./items.model');
const MAP_ITEM_REQ = require('./../../util/map_item_req');

function get(req, res, next) {
    const condition = {};
    condition.vendor = req.user._id;
    ItemModel
        .find(condition)
        .populate('vendor', {
            username: 1,
            email: 1
        })
        .exec(function (err, items) {
            if (err) {
                return next(err);
            }
            res.json(items);
        })

}
function getById(req, res, next) {
    ItemModel.findById(req.params.id, function (err, item) {
        if (err) {
            return next(err);
        }
        if (!item) {
            return next({
                msg: "Item not found",
                status: 404
            })
        }
        res.json(item);
    })
}
function post(req, res, next) {
    // insert
    console.log('req.body>>', req.body);
    console.log('req.file >>', req.file)
    console.log('req.files >>', req.files)

    const newItem = new ItemModel({});
    const data = req.body;
    if (req.file) {
        data.images = req.file.filename
    }
    if (req.files) {
        data.images = req.files.map(function (file) {
            return file.filename
        })
    }
    // TODO add properties in data
    data.vendor = req.user._id;

    MAP_ITEM_REQ(newItem, data);

    newItem.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.json(done);
    })

}
function update(req, res, next) {
    const data = req.body;
    if (req.file) {
        data.images = req.file.filename;
    }
    ItemModel.findById(req.params.id, function (err, item) {
        if (err) {
            return next(err);
        }
        if (!item) {
            return next({
                msg: "Item not found",
                status: 404
            })
        }
        // todo add data in req.body
        MAP_ITEM_REQ(item, data);

        item.save(function (err, updated) {
            if (err) {
                return next(err);
            }
            if (req.file) {
                // remove old images
            }
            res.json(updated);
        })

    })

}
function remove(req, res, next) {
    // if (req.user.role !== 1) {
    //     return next({
    //         msg: 'You Dont have Access',
    //         status: 403
    //     })
    // }
    ItemModel.findById(req.params.id, function (err, item) {
        if (err) {
            return next(err);
        }
        if (!item) {
            return next({
                msg: "Item not found",
                status: 404
            })
        }
        item.remove(function (err, removed) {
            if (err) {
                return next(err);
            }
            res.json(removed);
        })
    })

}
function search(req, res, next) {
    const data = req.method === 'POST' ? req.body : req.query;
    const searchCondition = {};

    MAP_ITEM_REQ(searchCondition, data);

    if (req.body.minPrice) {
        searchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        searchCondition.price = {
            $lte: req.body.maxPrice,
            $gte: req.body.minPrice
        }
    }

    if (req.body.fromDate && req.body.toDate) {
        const fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0); // milisecond
        const toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);

        searchCondition.createdAt = {
            $lte: new Date(toDate),
            $gte: new Date(fromDate)
        }
    }
    if (req.body.tags) {
        searchCondition.tags = {
            $in: req.body.tags.split(',')
        }
    }
    console.log('search condition >>', searchCondition)
    ItemModel
        .find(searchCondition)
        .populate('vendor')
        .populate('ratings.user')
        .then(function (items) {
            res.json(items)
        })
        .catch(function (err) {
            next(err);
        })


}
function addRatings(req, res, next) {
    ItemModel.findById(req.params.item_id, function (err, item) {
        if (err) {
            return next(err);
        }
        if (!item) {
            return next({
                msg: "Item not found",
                status: 404
            })
        }
        if (req.body.ratingMessage && req.body.ratingPoint) {
            let ratingData = {
                point: req.body.ratingPoint,
                message: req.body.ratingMessage,
                user: req.user._id
            }
            item.ratings.push(ratingData);
            item.save(function (err, saved) {
                if (err) {
                    return next(err);
                }
                res.json(saved);
            })
        } else {
            next({
                msg: "mesasge and point is required",
                status: 400
            })
        }

    })
}

module.exports = {
    get,
    getById,
    post,
    update,
    remove,
    search,
    addRatings
}
