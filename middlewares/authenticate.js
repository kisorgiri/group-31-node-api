const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    // console.log('req.headers', req.headers);
    let token;
    if (req.headers['authorization'])
        token = req.headers['authorization']
    if (req.headers['x-access-token'])
        token = req.headers['x-access-token']
    if (req.headers['token'])
        token = req.headers['token']
    if (req.query.token) {
        token = req.query.token
    }

    if (!token) {
        return next({
            msg: "Authentication Failed, Token Not Provided",
            status: 400
        })
    }
    // token verification
    jwt.verify(token, config.JWT_secret, function (err, decoded) {
        if (err) {
            return next(err);
        }
        // console.log('decoded >>', decoded);
        // req.user = decoded;
        // pass the control to next middleware
        UserModel.findById(decoded._id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Token no longer active please contatct system administrator",
                    status: 403
                })
            }
            req.user = user;
            next();
        })
    })

}