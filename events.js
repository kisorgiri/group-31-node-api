const events = require('events');
const myEvent = new events.EventEmitter();
const myEvent1 = new events.EventEmitter();



myEvent1.on('broadway', function (data) {
    console.log('this will be executed once broadway event is triggered',data);
})

setTimeout(function () {
    myEvent.emit('broadway','hi');

}, 400)
