const router = require('express').Router();
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../util/map_user_req');
const Uploader = require('./../middlewares/uploader');

router.route('/')
    .get(function (req, res, next) {
        // find all
        const condition = {};
        // project cannot have mix of inclusion and exclusion
        UserModel
            .find(condition)
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(1)
            .exec(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
    })
    .post(function (req, res, next) {

    });

router.route('/search')
    .get(function (req, res, next) {
        res.send('from user search')
    })
    .post(function (req, res, next) {

    });


router.route('/:id')
    .get(function (req, res, next) {
        UserModel
            .findById(req.params.id)
            .then(function (user) {
                if (!user) {
                    return next({
                        msg: 'User Not Found',
                        status: 404
                    })
                }
                res.json(user);
            })
            .catch(function (err) {
                next(err);
            })
    })
    .put(Uploader.single('image'), function (req, res, next) {
        const data = req.body;
        if (req.fileTypeError) {
            return next({
                msg: "Invalid file format",
                status: 400
            })
        }
        if (req.file) {
            data.image = req.file.filename
        }
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "User Not Found",
                    status: 404
                })
            }
            const oldImage = user.image;
            // user is mongoose object
            var updatedUser = MapUserReq(user, req.body)

            updatedUser.save(function (err, updated) {
                if (err) {
                    return next(err);
                }
                if (req.file) {
                    require('fs').unlink(require('path').join(process.cwd(), 'uploads/images/' + oldImage), function
                        (err, done) {
                        if (err) {
                            console.log('failed')
                        } else {
                            console.log('removed')
                        }
                    })
                }
                // TODO remove old picture from server if new picture is stored in db
                res.json(updated)
            })

        })

    })
    .delete(function (req, res, next) {
        UserModel.findById(req.params.id)
            .then(function (user) {
                if (user) {
                    user.remove(function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.json(done);
                    })
                } else {
                    next({
                        msg: "User Not Found",
                        status: 404
                    })
                }
            })
    });





module.exports = router;