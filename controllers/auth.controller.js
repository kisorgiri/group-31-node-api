const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const MapUserReq = require('./../util/map_user_req');
const Uploader = require('./../middlewares/uploader');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');
const nodemailer = require('nodemailer');

const sender = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
})

function prepareEmail(data) {
    return {
        from: 'Group 31 Store', // sender address
        to: "binod.foeva@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Forgot Password?", // plain text body
        html: `<p>Hi <strong>${data.name}</strong>,</p>
        <p>We noticed that you are having trouble logging into our system. please use link below to reset password
        </p>
        <p><a href="${data.link}">click here to reset password</a></p>
        <p>If you have not requested to change your password please kindly ignore this email</p>
        <p>Regards,</p>
        <p>Group31 Support Team</p>`, // html body
    }
}

function createToken(data) {
    var token = jwt.sign({
        _id: data._id,
        role: data.role,
        name: data.username
    }, config.JWT_secret);
    return token;
}

router.get('/', function (req, res, next) {

    require('fs').readFile('dslkfj.cdlkjf', function (err, done) {
        if (err) {
            req.myEvent.emit('err', err, res);
        }
    })
})

router.post('/login', function (req, res, next) {
    UserModel
        .findOne({
            $or: [
                { username: req.body.username },
                { email: req.body.username }
            ]
        })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Invalid Username",
                    status: 400
                })
            }
            if (user.status === 'inActive') {
                return next({
                    msg: "Your Account is disabled please contact system administrator for support",
                    status: 400
                })
            }
            // user exist now perform passwor verification
            var isMatched = passwordHash.verify(req.body.password, user.password);
            if (isMatched) {
                var authToken = createToken(user);
                res.json({
                    user: user,
                    token: authToken
                });
            } else {
                return next({
                    msg: "Invalid Password",
                    status: 400
                })
            }
        })

})

router.post('/register', Uploader.single('image'), function (req, res, next) {

    // data validation
    // TODO
    // library ==> JOI, express-validator
    console.log('req.body >>', req.body);
    console.log('req.file >>', req.file);
    const data = req.body;
    if (req.fileTypeError) {
        return next({
            msg: "Invalid file format",
            status: 400
        })
    }

    if (req.file) {
        // remove if you use file filter
        // var mimeType = req.file.mimetype;
        // var type = mimeType.split('/')[0];
        // if (type !== 'image') {
        //     // TODO remove file from server
        //     // hint use fs.unlink with req.file.filename
        //     return next({
        //         msg: 'Invalid File Format',
        //         status: 400
        //     })
        // }
        /////////////////////
        data.image = req.file.filename;
    }
    const newUser = new UserModel({});
    // mongoose object
    var newMappedUser = MapUserReq(newUser, data);
    newMappedUser.password = passwordHash.generate(req.body.password);

    // newUser.save(function (err, done) {
    //     if (err) {
    //         return next(err);
    //     }
    //     res.json(done);
    // })
    newMappedUser
        .save()
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        })

})


router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: "Email not Registered yet",
                status: 400
            })
        }
        const passwordResetToken = require('randomstring').generate(22);
        const passwordResetTokenExpiry = Date.now() + (1000 * 60 * 60 * 24 * 2);
        // email sending 
        const emailData = {
            name: user.username,
            email: user.email,
            link: `${req.headers.origin}/reset_password/${passwordResetToken}`
        }

        const email = prepareEmail(emailData);

        user.passwordResetToken = passwordResetToken;
        user.passwordResetTokenExpiry = passwordResetTokenExpiry
        user.save(function (err, done) {
            if (err) {
                return next(err);
            }
            sender.sendMail(email, function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })

    })

})

router.post('/reset-password/:token', function (req, res, next) {

    UserModel.findOne({
        passwordResetToken: req.params.token,
        // passwordResetTokenExpiry: {
        //     $gt: Date.now()
        // }
    }, function (err, user) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return next({
                msg: 'User Not Found',
                status: 404
            })
        }
        if (user.passwordResetTokenExpiry < Date.now()) {
            return next({
                msg: "Password reset token expired",
                status: 400
            })
        }
        user.password = passwordHash.generate(req.body.password);
        user.passwordResetToken = null;
        user.passwordResetTokenExpiry = null;
        user.save(function (err, done) {
            if (err) {
                return next(err);
            }
            res.json(done);
        })
    })
})


module.exports = router;
