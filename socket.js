const socket = require('socket.io');

module.exports = function (app) {
    const io = socket(app.listen(9001), {
        cors: {
            allow: '/*'
        }
    });
    io.on('connection', function (client) {
        console.log('Socket Client Connected to server');
        client.emit('hello', 'welcome to socket server');
        // 
        client.on('hi', function (data) {
            console.log('hi event in server >', data);
        })

        client.on('new-message', function (data) {
            // public chat 
            client.emit('reply-message-own', data);
            // emit is for own client
            client.broadcast.emit('reply-message-other', data);
            // requester bahek ka socket ka client

            // private chat
            client.broadcast.to('id').emit('event', data);
        })
    })
}
