// db connection

const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');
const conxnURL = dbConfig.conxnURL + '/' + dbConfig.dbName;
// mongodb://localhost:27017/group31db

mongoose.connect(conxnURL, {
    useUnifiedTopology: true,
    useNewUrlParser: true
})

mongoose.connection.once('open', function () {
    console.log('db connection success');
})

mongoose.connection.on('err', function (err) {
    console.log('db connection failed ', err)
})
