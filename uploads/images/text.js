Select this option if you don't want the system to distribute Supplier Charged Tax/Tax among line-items.
Tax will be calculated and auto filled based on the supplier charged tax/Tax and the Estimated tax and the user configurations in AP settings.
If the supplier charged tax/tax is higher than the Estimated tax, Tax field will be updated with the Estimated Tax If the supplier charged tax/Tax is lower than the Estimated tax, Tax field will be updated with the Estimated tax Automatically distribute Supplier Charged Tax/Tax between line itemsSet this to ‘On’ if you want the system to automatically distribute the total Supplier Charged Tax/Tax among line - items of an invoice or memoWhen the invoice is created, the system automatically distributes / splits the total Supplier Charged Tax/Tax among the applicable line - items of the invoice, based on the selected option.
The system automatically distributes / splits the total Supplier Charged Tax/Tax among the applicable line - items of an invoice or memo based on the selected option, immediately after the user manually enters the total Supplier Charged Tax/Tax on the ‘Supplier Charged Tax/Tax’ field.
Choose how would you like to distribute Supplier Charged Tax/Tax below: (only applicable to taxable line items) Note: If there is no tax code specified, the system will not distribute the Supplier Charged Tax/Tax on the line item.
Automatically distribute when Supplier submits Supplier Charged Tax/Tax at line - item levelSet this to ‘On’ if you want the system to automatically distribute the total Supplier Charged Tax/Tax among the applicable line - items of an invoice or memo, even if the Supplier submits Supplier Charged Tax/Tax at the line - item level.
If this setting is ‘On’ and the Supplier submits Supplier Charged Tax/Tax at the line - item level, the system performs Tax distribution onto the applicable line - items which will most likely override the Supplier Charged Tax/Tax submitted by the Supplier.
If this setting is ‘On’ and the Supplier < strong > does not</strong > submit Supplier Charged Tax/Tax at the line - item level, the system performs Supplier Charged Tax/Tax distribution onto the applicable line - items.
If this setting is ‘Off’ and the Supplier submits Supplier Charged Tax/Tax at the line - item level, the system < strong > does not</strong > perform Supplier Charged Tax/Tax distribution at allIf this setting is ‘Off’ and the Supplier < strong > does not</strong > submit Supplier Charged Tax/Tax at the line - item level, the system performs Supplier Charged Tax/Tax distribution onto the applicable line - items.


Select this option if you want the system to distribute total Supplier Charged Tax/Tax based on the proportion of the line - item Total(includes Shipping Charges, if present) to the Invoice Subtotal.
</br ></br > Distributed Supplier Charged Tax/Tax on a line - item =[(line - item total + Shipping Charges) / (Subtotal + Total Shipping Charges) ] * Total Supplier Charged Tax/Tax

Select this option if you want the system to distribute total Supplier Charged Tax/Tax into the first ‘taxable’ line - item on the invoice / memo, from the top(sequentially).


Select this option if you want the system to distribute / split total Supplier Charged Tax/Tax equally among all ‘taxable’ line - items on an invoice / memo.
